# temporal-hours

A small script to display the time according to the pre-modern custom of dividing the night and day into 12 hours each. The length of each hour depends on the time of year and your geographic location. Uses the https://api.sunrisesunset.io/ API.

**Wait, what?** 

Before the development of very precise mechanical clocks (and long before the need for time zones), many societies divided the night and day into 12 equal parts each. This meant that, for example, during the northern hemisphere's wintertime, daytime hours were shorter than nighttime hours, and vice versa during summer. Daytime and nighttime hours were exactly equal at the equinoxes, and gradually shifted to their most unequal lengths at the solstices. 

**But what did they do before bash scripts?**

They used devices like sundials or astrolabes, designed specifically for a particular location. There are many guides to making and using your own astrolabe, e.g.:

- https://in-the-sky.org/astrolabe/index.php
- https://www.eso.org/~ohainaut/bin/astrolabe.cgi
- https://sourcebooks.fordham.edu/source/chaucer-astro.asp

Some more reading:

- https://en.wikipedia.org/wiki/Unequal\_hours
- http://web.archive.org/web/20160219224638/https://www.scientificamerican.com/article/a-chronicle-of-timekeeping-2006-02/
- https://jamesbshannon.com/2015/06/24/the-medieval-concept-of-time/ 

## Requirements

- `bash`
- `curl`
- `jq` 
- the GNU coreutils version of `date` (check yours with `date --version`)

## Installation

Save the script to your computer. Make it executable with `chmod +x ~/the/folder/you/put/it/in/temporal-hours.sh`

Optionally, move it to a folder in your `$PATH` (e. g. `~/.local/bin`; check `~/.bash_profile`) and make sure your computer knows about it by doing `. .bash_profile`.

## Configuration

Open the file in a text editor and replace the co-ordinates in the "Co-ordinates" section with your preferred location's. You can also customize the default output by uncommenting/editing lines in the "Output" section.

## Usage 

Enter `./temporal-hours.sh` at the command line. If the script is in your `$PATH`, simply enter `temporal-hours.sh`. 
Command-line options:
    -h, --help	     Show this help message.
    -b, --basic      Show hours, minutes, and "day/night".
    -c, --clock      Show a clock with day/night emoji, hours, minutes, and hour length.
    -d, --debug      Print values of several variables. For testing purposes.
    -n, --numeric    Show a digital-style clock with hours, minutes, seconds, and day/night.
    -w, --words      Show the date, time, hour length, and next day/night's hour length in sentences.

Without any arguments, `temporal-hours.sh` shows hours, minutes, day/night, hour length, and the next day/night's hour length, e.g.: 

```
09:10 (day)
This hour has 58 minutes.
Nighttime hours will be 70 minutes long.
```

For a fancy display, pipe the basic output to a program like [figlet](http://www.figlet.org/) or [lolcat](https://github.com/busyloop/lolcat).

For a clock that updates in real time, enter `watch -t -n[time] ./temporal-hours.sh [argument]`, where the number after n is the interval in seconds. `n0.1` (the minimum) works well if you're displaying time with seconds. `n300` will update every 5 minutes. Exit with `Ctrl+C`.

## To do

- [x] Turn into a live clock you can just leave running in a window or whatever. 
- [ ] Make code more compact/efficient
- [x] Prettify output, e. g. with colours, box drawing, or ASCII text art
- [x] Fix seconds (it rolls over at 60 regardless of minute length).
