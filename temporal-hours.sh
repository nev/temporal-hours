#!/bin/bash

# In this script
# ==============
# 1. Co-ordinates
# 2. Functions
# 3. Time variables
# 4. Calculations
# 5. Command-line options
# 6. Default output

# Co-ordinates
# ============

# Currently set to Toronto City Hall. Substitute with your own location.

lat=43.652473
long=-79.385068

# Functions
# =========

usage () {
	printf '%s\n' "temporal-hours.sh: Display the time in relative hours.
Usage: temporal-hours.sh [option]

Options:
    -h, --help	     Show this help message.
    -b, --basic      Show hours, minutes, and \"day/night\".
    -c, --clock      Show a clock with day/night emoji, hours, and minutes.
    -n, --numeric    Show a digital-style clock with hours, minutes, seconds, 
                     and day/night.
    -w, --words      Show the date, time, hour length, and the next day/night's
                     hour length in sentences.

Before the development of very precise mechanical clocks (and long before the 
need for time zones), many societies divided the night and day into 12 equal 
parts each. This meant that, for example, during the northern hemisphere's 
wintertime, daytime hours were shorter than nighttime hours, and vice versa 
during summer. Daytime and nighttime hours were exactly equal at the equinoxes,
and gradually shifted to their most unequal lengths at the solstices. 

This script calculates time based on this system, using the sunrisesunset.io 
API. Time and hour length depend on your location. The default location is 
Toronto, Canada. Set your location by editing the latitude and longitude values
in temporal-hours.sh.

Without any arguments, temporal-hours.sh shows the time in hours and minutes, 
the current hour length, and the next day's/night's hour length. You can 
customize the output by editing the \"Default output\" section of temporal-hours.sh."
}

# Time variables
# ==============

now=$(date +'%s')

# Is there a more efficient way to do this? Absolutely. Do I know it? NOPE

sunrise=$(date --date "$(curl -s "https://api.sunrisesunset.io/json?lat=$lat&lng=$long" | jq -r '.results.sunrise')" +'%s')
sunset=$(date --date "$(curl -s "https://api.sunrisesunset.io/json?lat=$lat&lng=$long" | jq -r '.results.sunset')" +'%s')
prevSunrise=$(date --date "yesterday $(curl -s "https://api.sunrisesunset.io/json?lat=$lat&lng=$long&date=yesterday" | jq -r '.results.sunrise')" +'%s')
prevSunset=$(date --date "yesterday $(curl -s "https://api.sunrisesunset.io/json?lat=$lat&lng=$long&date=yesterday" | jq -r '.results.sunset')" +'%s')
nextSunrise=$(date --date "tomorrow $(curl -s "https://api.sunrisesunset.io/json?lat=$lat&lng=$long&date=tomorrow" | jq -r '.results.sunrise')" +'%s')
nextSunset=$(date --date "tomorrow $(curl -s "https://api.sunrisesunset.io/json?lat=$lat&lng=$long&date=tomorrow" | jq -r '.results.sunset')" +'%s')

# Calculations 
# ============ 

# Day or night
# ------------ 

if (( $now < $sunrise ))
	then
		period="night"
		periodStart=$prevSunset
		periodEnd=$sunrise
		nextPeriod="day"
		nextPeriodStart=$sunrise
		nextPeriodEnd=$nextSunset
elif (( $now >= $sunrise && $now < $sunset ))
	then
		period="day"
		periodStart=$sunrise
		periodEnd=$sunset
		nextPeriod="night"
		nextPeriodStart=$sunset
		nextPeriodEnd=$nextSunrise
elif (( $now >= $sunset ))
	then
		period="night"
		periodStart=$sunset
		periodEnd=$nextSunrise
		nextPeriod="day"
		nextPeriodStart=$nextSunrise
		nextPeriodEnd=$nextSunset
else
	echo "lol idk"
fi

if [ $period == "night" ]
	then 
		emojo="🌝 "
else
	emojo="🌞"
fi

# Values & variables
# ------------------

periodLength=$(( periodEnd - periodStart ))         # Length of current day/night in seconds
periodAge=$(( now - periodStart ))                  # How far we are into the current period in seconds
hourLength=$(( periodLength / 12 ))                 # Hour length in seconds
hourLengthMins=$(( hourLength / 60 ))               # Hour length in minutes

hours=$(( periodAge / hourLength  ))                # Current hour
minutes=$(( ( periodAge % hourLength ) / 60 ))      # Current minute
seconds=$(( ( periodAge % hourLength ) % 60 ))      # Current second

nextPeriodLength=$(( nextPeriodEnd - nextPeriodStart )) # Length of next day/night in seconds
nextHourLength=$(( nextPeriodLength / 12 ))         # Next hour length in seconds
nextHourLengthMins=$(( nextHourLength / 60 ))       # Next hour length in minutes

printf -v clockHours "%02d" $hours                  # Zero-padding for a digital-type clock display.
printf -v clockMins "%02d" $minutes
printf -v clockSecs "%02d" $seconds

periodUpper=${period^^}                             # Uppercase period

# Command-line options
# ====================

while test $# -gt 0; do
	case "$1" in
		-h|--help)
			usage
			exit 0
		;;
		-b|--basic)
			# Basic output:
			# 1:20 (day/night)
		  printf "%s:%s (%s)\n" "$hours" "$clockMins" "${period}" 
			exit 0
		;;
		-c|--clock)
			# Pretty clock:
			# ╭──────────╮
			# │ 🌞 HH:MM │
			# │ H: 00:MM │
			# ╰──────────╯ 
			printf "╭──────────╮\n│ %s %s:%s │\n│ H: 00:%s │\n╰──────────╯\n" "$emojo" "$clockHours" "$clockMins" "$hourLengthMins"
			exit 0
		;;
		-n|--numeric)
			# Digital clock-like format:
			# 01:20:30 (night)
			printf "%s:%s:%s " "$clockHours" "$clockMins" "$clockSecs"
			printf "(%s)\n" "${period}"
			exit 0
		;;
		-w|--words)
			# Time & hour length in words:
			# Thursday, November 9, 2023 
			# Nighttime hours are 70 minutes long.
			# It is about 6:25. <sun/moon emoji>
			echo "$(date +'%A, %B %-d, %Y')"
			echo "It is about $hours:$clockMins. $emojo"
			echo "${period^}time hours are $hourLengthMins minutes long."
			echo "${nextPeriod^}time hours will be $nextHourLengthMins minutes long."
			exit 0
		;;
		*)
			printf '%s\n' "Option not recognized. Use -h or --help for usage options."
			exit 1
		;;
	esac
done

# Default output
# ==============

# Different display styles and formats. Simply uncomment the ones you want to use and edit as desired.
# For a live clock, run `watch -t -n[time] ./temporal-hours.sh [argument]`. `n0.1` works for every few seconds, `n300` for every 5 minutes. Exit with Ctrl+C.
# For a fancy display, you can pipe the basic output to a program like figlet or lolcat.

# Out of the box
# --------------

# printf "%(%A %B %-d, %Y)T\n"                                  # Date in words ("Thursday November 9, 2023"
# printf "%(%F)T\n"                                             # Date in YYYY-MM-DD format.
printf "%s:%s (%s)\n" "$hours" "$clockMins" "$period"         # Time in numbers
printf "%s\n" "This hour has $hourLengthMins minutes."        # Hour length
printf "%s\n" "${nextPeriod^}time hours will be $nextHourLengthMins minutes long."

# Time with seconds
# -----------------

# printf "%s:%s:%s (%s)\n" "$hours" "$clockMins" "$clockSecs" "$period"   # H:MM:SS (day/night)

# Date & time in words
# --------------------

# echo "$(date +'%A, %B %-d %Y')"       # Thursday, November 9, 2023
# echo "${period^} hours are $hourLengthMins minutes long."
# echo "${nextPeriod^} hours will be $nextHourLengthMins minutes long."
# echo "It is about $clockHours:$clockMins. $emojo"

# Pretty clock:
# ╭─────────────╮
# │ 🌞 HH:MM:SS │
# │    H: 00:MM │
# ╰─────────────╯ 

# printf "╭─────────────╮\n│ %s %s:%s:%(%S)T │\n│    H: 00:%s │╰─────────────╯\n" "$emojo" "$clockHours" "$clockMins" "$hourLengthMins"
